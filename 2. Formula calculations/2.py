from math import sin, hypot, pi, cos


def t2_1_1(x: int) -> int:
    """
    Calculating 'y' from formula
    """
    # 18.1
    y = 17 * x ** 2 - 6 * x + 13
    return y


def t2_1_2(a: int) -> int:
    """
    Calculating 'y' from formula
    """
    # 18.2
    y = 3 * a ** 2 + 5 * a - 21
    return y


def t2_2(a: int) -> int:
    """
    Calculating 'x' from formula
    """ 
    # 19
    x = (a ** 2 + 10) / (a ** 2 + 1) ** 0.5
    return x


def t2_3_1(a: int) -> float:
    """
    Calculating 'x' from formula
    """
    # 20.1
    x = ((2 * a + sin(abs(3 * a))) / 3.56) ** 0.5
    return x


def t2_3_2(x: int) -> float:
    """
    Calculating 'y' from formula
    """
    # 20.2
    y = sin((3.2 + (1 + x) ** 0.5) / abs(5 * x))
    return y


def t2_4(side: int) -> int:
    """
    Calculating perimeter
    """
    # 21
    x = 4 * side
    return x


def t2_5(radius: int) -> int:
    """
    Calculating diameter
    """
    # 22
    diameter = radius * 2
    return diameter


def t2_6(height: int) -> int:
    """
    Calculating distance from a given height point to horizon line
    """
    # 23
    radius = 6350
    x = ((radius + height) ** 2 - radius ** 2) ** 0.5
    return x


def t2_7(length: int) -> str:
    """
    Calculating volume and area
    """
    # 24
    volume = length ** 3
    area = 6 * length ** 2
    result = f"Volume: {volume}. Area: {area}."
    return result


def t2_8(radius: int) -> str:
    """
    Calculating circle length and area
    """
    # 25
    length = pi * radius * 2
    area = pi * (2 * radius) ** 2 / 4
    result = f"Length: {length}. Area: {area}."
    return result


def t2_9_1(x: int, y: int) -> int:
    """
    Calculating 'z' from formula
    """
    # 26.1
    z = 2 * x ** 3 - 3.44 * x * y + 2.3 * x ** 2 - 7.1 * y + 2
    return z


def t2_9_2(a: int, b: int) -> int:
    """
    Calculating 'x' from formula
    """
    # 26.2
    x = 3.14 * (a + b) ** 3 + 2.75 * b ** 2 - 12.7 * a - 4.1
    return x


def t2_10(a: int, b: int) -> str:
    """
    Calculating geometric and arithmetic mean
    """
    # 27
    arithmetic = (a + b) / 2
    geometric = (a * b) ** 0.5
    result = f"Arithmetic: {arithmetic}. Geometric: {geometric}."
    return result


def t2_11(volume: int, weight: int) -> float:
    """
    Calculating density
    """
    # 28
    density = weight / volume
    return density


def t2_12(population: int, area: int) -> float:
    """
    Calculating population density
    """
    # 29
    density_population = population / area
    return density_population


def t2_13(a: int, b: int) -> str | float:
    """
    Calculating linear equation
    ax + b = 0 (a != 0)
    """
    # 30
    if a == 0:
        return "'a' can't be a zero"
    else:
        x = (-b / a)
    return x


def t2_14(a: int, b: int):
    """
    Calculating hypotenuse
    """
    # 31
    c = hypot(a, b)
    return c


def t2_15(r_external: int, r_internal: int) -> int:
    """
    Calculating ring area
    """
    # 32
    area = pi * (r_external ** 2 - r_internal ** 2)
    return area


def t2_16(a: int, b: int) -> float:
    """
    Calculating perimeter of right triangle
    """
    # 33
    hypotenuse = hypot(a, b)
    perimeter = hypotenuse + a + b
    return perimeter


def t2_17(base1: int, base2: int, height: int) -> int:
    """
    Calculating perimeter of an isosceles trapezoid
    """
    # 34
    perimeter = height * 2 + base1 + base2
    return perimeter


def t2_18(x: int, y: int) -> tuple:
    """
    Calculating 'z' and 'q' from formula
    """
    # 35
    z = (x + (2 + y / x ** 2)) / (y + (1 / (x ** 2 + 10)))
    q = 7.25 * sin(x) - abs(y)
    return z, q


def t2_19(a: int, b: int) -> tuple:
    """
    Calculating 'x' and 'y' from formula
    """
    # 36
    x = (2 / (a ** 2 + 25)) + b / (b ** 0.5 + ((a + b) / 2))
    y = (abs(a) + 2 * sin(b)) / 5.5 * a
    return x, y


def t2_20(e: int, f: int, g: int, h: int) -> tuple:
    """
    Calculating 'a', 'b' and 'c' from formula
    """
    # 37
    a = ((abs(e - (3 / f))) ** 3 + g) ** 0.5
    b = sin(e) + (cos(h)) ** 2
    c = (33 * g) / (f * e - 3)
    return a, b, c


def t2_21(e: int, f: int, g: int, h: int) -> tuple:
    """
    Calculating 'a', 'b' and 'c' from formula
    """
    # 38
    a = (e + (f / 2)) / 3
    b = abs(h ** 2 - g)
    c = ((g - h) ** 2 - (3 * sin(e))) ** 0.5
    return a, b, c


def t2_22(a: int, b: int) -> str:
    """
    Calculating geometric and arithmetic mean modulo
    """
    # 39
    arithmetic = (abs(a) + abs(b)) / 2
    geometric = (abs(a) * abs(b)) ** 0.5
    result = f"Arithmetic mean modulo: {arithmetic}. Geometric mean modulo: {geometric}."
    return result


def t2_23(height: int, width: int) -> str:
    """
    Calculating perimeter and diagonal of rectangle
    """
    # 40
    perimeter = 2 * (height + width)
    diagonal = (height ** 2 + width ** 2) ** 0.5
    result = f"Rectangle - Perimeter: {perimeter}. Diagonal: {diagonal}"
    return result


def t2_24(a: int, b: int) -> str:
    """
    Calculating sum, difference, product and division
    """
    # 41
    if b == 0:
        return "'b' cannot be null"
    else:
        return f"Sum - {a + b}, Difference - {a - b}, Product - {a * b}, Division - {a / b}"


def t2_25(base1: int, base2: int, height: int) -> str:
    """
    Calculating volume and area aide surface of cuboid
    """
    # 42
    volume = base1 * base2 * height
    area = 2 * height * (base1 + base2)
    return f"Cuboid volume: {volume}, Area side surface: {area}"
    