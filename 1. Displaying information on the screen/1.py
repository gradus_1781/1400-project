from math import pi, e


def t1_1():
    """
    Prints numbers with 1 space between them
    """
    # 1
    print(31, 18, 79)


def t1_2():
    """
    Prints numbers with 2 space between them
    """
    # 2
    print(47, 52, 150, sep="  ")


def t1_3():
    """
    Prints each numbers on new line
    """
    # 3
    print(50, 10, sep="\n")


def t1_4():
    """
    Prints each numbers on new line
    """
    # 4
    print(5, 10, 21, sep="\n")


def t1_5():
    """
    Prints each numbers on new line
    """
    # 5
    print(1, 2, sep="\n")


def t1_6() -> float:
    """
    Return const 'pi' with 3 decimal places
    """
    # 6
    result = round(pi, 3)
    return result


def t1_7() -> float:
    """
    Return const 'e' with 1 decimal places
    """
    # 7
    result = round(e, 1)
    return result


def t1_8() -> str:
    """
    Return input text with additional words
    """
    # 8
    number = input()
    result = f"Вы ввели число {number}"
    return result


def t1_9() -> str:
    """
    Return input text with additional words
    """
    # 9
    number = input()
    result = f"{number} - вот какое число вы ввели"
    return result


def t1_10() -> str:
    """
    Return input text with additional words
    """
    # 10
    text = input("Какое у вас имя? ")
    result = f"Ваше имя {text}"
    return result


def t1_11() -> str:
    """
    Return input text with additional words
    """
    # 11
    text = input("Название футбольной команды? ")
    result = f"{text} - это чемпион!"
    return result


def t1_12() -> str:
    """
    Return input text with additional words
    """
    # 12
    text = input()
    result = f"Привет, {text}!"
    return result


def t1_13():
    """
    Prints next and previous number of input number
    """
    # 13
    num = int(input())
    print(f"Следующее за числом {num} число – {num + 1}.")
    print(f"Для числа {num} предыдущее число – {num - 1}.")


def t1_14():
    """
    Prints input numbers with 2 space between them
    """
    # 14
    text = input().split()
    print(*text, sep="  ")


def t1_15():
    """
    Prints numbers with 1 space between them
    """
    # 15
    text = input()
    print(text)


def t1_16_1():
    """
    Prints numbers to a given template
    """
    # 16.1
    print("5 10")
    print("7 см")


def t1_16_2():
    """
    Prints numbers to a given template
    """
    # 16.2
    t, v = input().split()
    print(f"100 {t}")
    print(f"1949 {v}")


def t1_16_3():
    """
    Prints numbers to a given template
    """
    # 16.3
    x, y = input().split()
    print(f"{x} 25")
    print(f"{x} {y}")


def t1_17_1():
    """
    Prints numbers to a given template
    """
    # 17.1
    print("2 кг")
    print("13 17")


def t1_17_2():
    """
    Prints numbers to a given template
    """
    # 17.2
    a, b = input().split()
    print(f"{a} 1")
    print(f"19 {b}")


def t1_17_3():
    """
    Prints numbers to a given template
    """
    # 17.3
    x, y = input().split()
    print(f"{x} {y}")
    print(f"5 {y}")
